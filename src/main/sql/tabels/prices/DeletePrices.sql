DELETE FROM prices
WHERE price = 5999.99 AND effective_date = TO_DATE('2012-09-15', 'yyyy-mm-dd');

DELETE FROM prices
WHERE price = 1112.0 AND effective_date = TO_DATE('2015-01-19', 'yyyy-mm-dd');

DELETE FROM prices
WHERE price = 59.99 AND effective_date = TO_DATE('2014-11-10', 'yyyy-mm-dd');

DELETE FROM prices
WHERE price = 39.99 AND effective_date = TO_DATE('2016-03-20', 'yyyy-mm-dd');