-- Задание: Вывести все транзакции за 2015 год добавляя префикс "Покупка "

SELECT
  'Покупка ' || transaction_id,
  client_id,
  goods_id,
  date_time
FROM transactions
WHERE date_time BETWEEN TO_DATE('2015-01-01', 'yyyy-mm-dd') AND TO_DATE('2015-12-31', 'yyyy-mm-dd');